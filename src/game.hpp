// Copyright 2017, Stijn Brouwer
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef LIERO_GAME_HPP
#define LIERO_GAME_HPP

#include <memory>
#include <math.h>
#include <glm/glm.hpp>

struct Terrain {
    // The terrain is subdivided into uniformly sized chunks in order to minimise
    // the amount of data that needs to be sent over the network, and uploaded to
    // the GPU whenever the terrain gets modified.
    static constexpr int CHUNK_SIZE = 32;

    // For the chunk optimisation to work, we need to cap off the SDF's values
    // at a certain maximum.
    static constexpr float MAX_DISTANCE = CHUNK_SIZE / 2;

    // The size of the terrain (in chunks, not texels)
    int width, height;
    // The signed distance field representing our terrain
    std::unique_ptr<float[]> texels;
    // Dirty bits for each chunk to signify which chunks are out-of-date on
    // the GPU and need to be reuploaded
    std::unique_ptr<bool[]> dirtyBits;

    // Construct a new terrain (width and height are in chunks, not texels)
    Terrain(int width, int height);

    // Obtain a reference to the SDF texel at the given location.
    float &index(int x, int y);
    float index(int x, int y) const;

    // Sample the SDF using bilinear interpolation.
    float sample(glm::vec2 point) const;
    // Calculate the SDF's gradient at the given point.
    // In practice, this returns the surface normal vector if the point is on the surface.
    glm::vec2 gradient(glm::vec2 point) const;
    // Same as gradient, but rotated 90 degrees counter-clockwise.
    // In practice, this is the tangent line of the isocurve through the given point.
    glm::vec2 tangent(glm::vec2 point) const;
    // Find the point closest to the one given on the isocurve of the given isovalue.
    glm::vec2 seekIsocurve(glm::vec2 point, float isovalue) const;
    // Solve `sample(point + c * normal) = isovalue` for c
    float seekIsocurve(glm::vec2 point, glm::vec2 normal, float isovalue) const;

    // Carve a crater (a bunch of circles) into the terrain
    void carveCrater(int circleCount, const glm::vec2 *centers, const float *radii);
    // Carve a single circle into the terrain
    void carveCircle(glm::vec2 center, float radius);
};

struct Ballistics {
    static constexpr float GRAVITY = 100;
};

struct Projectile {
    bool dead;
    glm::vec2 center;
    float radius;
    float explosivePower;
    glm::vec2 velocity;

    static void simulate(int projectileCount, Projectile *projectiles, Terrain *terrain, float timeStep);
};

struct PlayerCommands {
    bool moveLeft, moveRight;
    bool jump;
};

struct Player {
    static constexpr float JUMP_VELOCITY = 55;
    static constexpr float JUMP_SURFACE_ANGLE_CONTRIB = 0.4f;
    static constexpr float JUMP_GRACE_PERIOD = 0.3f;
    static constexpr float MOVEMENT_ACCELERATION = 300;
    static constexpr float MOVEMENT_DRAG = 5;
    static constexpr float STRUGGLE_ANGLE = 3.14f / 3;
    static constexpr float FALL_OFF_LEDGE_VELOCITY = 7;
    static constexpr float ON_GROUND_THRESHOLD = 0.50f;
    static constexpr float WALLJUMP_THRESHOLD = 0.50f;

    enum struct MovementMode {
        BALLISTIC,
        GROUND,
    };

    glm::vec2 center;
    float radius;

    MovementMode movementMode;
    glm::vec2 ballisticVelocity;
    float groundVelocity;
    float jumpGraceTimer;

    static void simulate(int playerCount, Player *players, const PlayerCommands *commands, const Terrain *terrain, float timeStep);
};

#endif
