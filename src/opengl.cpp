// Copyright 2017, Stijn Brouwer
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "opengl.hpp"
#include <array>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include "glad.h"
#include "game.hpp"

namespace {
    unsigned compileShader(GLenum type, const char *source) {
        unsigned shader = glCreateShader(type);
        glShaderSource(shader, 1, &source, nullptr);
        glCompileShader(shader);
        // TODO: Error checking
        return shader;
    }

    unsigned linkProgram(unsigned vert, unsigned frag) {
        unsigned program = glCreateProgram();
        glAttachShader(program, vert);
        glAttachShader(program, frag);
        glLinkProgram(program);
        // TODO: Error checking
        return program;
    }

    const char *vertexShaderSource = R"GLSL(
        #version 330 core
        layout (location = 0) in vec3 position;
        layout (location = 1) in vec3 color;
        layout (location = 2) in vec2 texCoord;
        out vec3 vColor;
        out vec2 vTexCoord;
        uniform mat4 uTransform;
        void main() {
            gl_Position = uTransform * vec4(position, 1.0);
            vColor = color;
            vTexCoord = texCoord;
        }
    )GLSL";

    const char *untexturedFragmentShaderSource = R"GLSL(
        #version 330 core
        in vec3 vColor;
        out vec4 outColor;
        void main() {
            outColor = vec4(vColor, 1.0);
        }
    )GLSL";

    const char *sdfFragmentShaderSource = R"GLSL(
        #version 330 core
        in vec3 vColor;
        in vec2 vTexCoord;
        out vec4 outColor;
        uniform sampler2D uTexture;
        const float isoValue = 0.0;
        const float smoothing = 0.15;
        void main() {
            float distance = texture(uTexture, vTexCoord).r;
            float alpha = smoothstep(isoValue - smoothing, isoValue + smoothing, distance);
            outColor = vec4(vColor, alpha);
            //outColor = vec4(sin(distance) * 0.5 + 0.5, alpha, alpha, 1.0);
        }
    )GLSL";
}

Gfx::Gfx() {
    glGenTextures((int) Texture::COUNT, &textures[0]);
    glGenVertexArrays(1, &vertexArray);
    glBindVertexArray(vertexArray);
    glGenBuffers(1, &vertexBuffer);
    glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
    glBufferData(GL_ARRAY_BUFFER, VERTEX_BUFFER_CAP, nullptr, GL_STREAM_DRAW);
    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);
    glEnableVertexAttribArray(2);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void *) (0 * sizeof(float)));
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void *) (3 * sizeof(float)));
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void *) (6 * sizeof(float)));
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    vertexShader = compileShader(GL_VERTEX_SHADER, vertexShaderSource);
    untexturedFragmentShader = compileShader(GL_FRAGMENT_SHADER, untexturedFragmentShaderSource);
    untexturedProgram = linkProgram(vertexShader, untexturedFragmentShader);
    sdfFragmentShader = compileShader(GL_FRAGMENT_SHADER, sdfFragmentShaderSource);
    sdfProgram = linkProgram(vertexShader, sdfFragmentShader);
    glBindVertexArray(0);
}

Gfx::~Gfx() {
    glDeleteTextures((int) Texture::COUNT, &textures[0]);
    glDeleteVertexArrays(1, &vertexArray);
    glDeleteBuffers(1, &vertexBuffer);
    glDeleteShader(vertexShader);
    glDeleteShader(untexturedFragmentShader);
    glDeleteProgram(untexturedProgram);
}

void Gfx::updateTerrain(Terrain *terrain, bool force) {
    glBindTexture(GL_TEXTURE_2D, textures[(int) Texture::TERRAIN_SDF]);
    if (force) {
        glTexImage2D(
            GL_TEXTURE_2D, 0, GL_R32F, terrain->width * Terrain::CHUNK_SIZE,
            terrain->height *Terrain::CHUNK_SIZE, 0, GL_RED, GL_FLOAT,
            (void *) &terrain->texels[0]
        );
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    }
    else {
        float buffer[Terrain::CHUNK_SIZE * Terrain::CHUNK_SIZE];
        for (int y = 0; y < terrain->height; ++y) {
            for (int x = 0; x < terrain->width; ++x) {
                if (terrain->dirtyBits[x + y * terrain->width]) {
                    for (int cy = 0; cy < Terrain::CHUNK_SIZE; ++cy) {
                        float *dest = &buffer[cy * Terrain::CHUNK_SIZE];
                        int srcx = x * Terrain::CHUNK_SIZE;
                        int srcy = cy + y * Terrain::CHUNK_SIZE;
                        int srcpitch = terrain->width * Terrain::CHUNK_SIZE;
                        float *src = &terrain->texels[srcx + srcy * srcpitch];
                        size_t len = sizeof(float) * Terrain::CHUNK_SIZE;
                        memcpy(dest, src, len);
                    }
                    glTexSubImage2D(
                        GL_TEXTURE_2D, 0, x * Terrain::CHUNK_SIZE, y * Terrain::CHUNK_SIZE,
                        Terrain::CHUNK_SIZE, Terrain::CHUNK_SIZE, GL_RED, GL_FLOAT,
                        (void *) buffer
                    );
                    terrain->dirtyBits[x + y * terrain->width] = false;
                }
            }
        }
    }
    glBindTexture(GL_TEXTURE_2D, 0);
}

void Gfx::clear() {
    vertexStack.clear();
}

void Gfx::pushVertex(Vertex vertex) {
    vertexStack.push_back(vertex);
}

void Gfx::flushUntextured(glm::mat4 transform) {
    if (vertexStack.empty()) return;

    glBindVertexArray(vertexArray);
    glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
    glBufferSubData(GL_ARRAY_BUFFER, 0, vertexStack.size() * sizeof(Vertex), (void *) &vertexStack[0]);

    glUseProgram(untexturedProgram);

    glUniformMatrix4fv(glGetUniformLocation(untexturedProgram, "uTransform"), 1, GL_FALSE, glm::value_ptr(transform));

    glDrawArrays(GL_TRIANGLES, 0, vertexStack.size());
    glBindVertexArray(0);
}

void Gfx::flushSDF(glm::mat4 transform, Texture texture) {
    if (vertexStack.empty()) return;

    glBindVertexArray(vertexArray);
    glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
    glBufferSubData(GL_ARRAY_BUFFER, 0, vertexStack.size() * sizeof(Vertex), (void *) &vertexStack[0]);

    glUseProgram(sdfProgram);

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, textures[(int) texture]);

    glUniformMatrix4fv(glGetUniformLocation(sdfProgram, "uTransform"), 1, GL_FALSE, glm::value_ptr(transform));

    glUniform1i(glGetUniformLocation(sdfProgram, "uTexture"), 0);

    glDrawArrays(GL_TRIANGLES, 0, vertexStack.size());
    glBindVertexArray(0);
}

void Gfx::setColor(glm::vec3 color) {
    currentColor = color;
}

void Gfx::quad(float x, float y, float rx, float ry, float z) {
    Vertex a, b, c, d;
    a.color = b.color = c.color = d.color = currentColor;
    a.position.z = b.position.z = c.position.z = d.position.z = z;
    a.position.x = x - rx; b.position.x = x - rx; c.position.x = x + rx; d.position.x = x + rx;
    a.position.y = y - ry; b.position.y = y + ry; c.position.y = y + ry; d.position.y = y - ry;
    a.texCoord.x = a.texCoord.y = 0;
    b.texCoord.x = 0; b.texCoord.y = 1;
    c.texCoord.x = c.texCoord.y = 1;
    d.texCoord.x = 1; d.texCoord.y = 0;
    pushVertex(c); pushVertex(b); pushVertex(a);
    pushVertex(a); pushVertex(d); pushVertex(c);
}

void Gfx::quadSDFAligned(float x, float y, float rx, float ry, int w, int h, float z) {
    Vertex a, b, c, d;
    a.color = b.color = c.color = d.color = currentColor;
    a.position.z = b.position.z = c.position.z = d.position.z = z;
    a.position.x = x - rx; b.position.x = x - rx; c.position.x = x + rx; d.position.x = x + rx;
    a.position.y = y - ry; b.position.y = y + ry; c.position.y = y + ry; d.position.y = y - ry;
    a.texCoord.x = 0.5f / w; a.texCoord.y = 0.5f / h;
    b.texCoord.x = 0.5f / w; b.texCoord.y = 1 + 0.5f / h;
    c.texCoord.x = 1 + 0.5f / w; c.texCoord.y = 1 + 0.5f / h;
    d.texCoord.x = 1 + 0.5f / w; d.texCoord.y = 0.5f / h;
    pushVertex(c); pushVertex(b); pushVertex(a);
    pushVertex(a); pushVertex(d); pushVertex(c);
}

void Gfx::circle(float x, float y, float r, float z, int verts) {
    Vertex center(x, y, z, currentColor.r, currentColor.g, currentColor.b, 0, 0);
    float angleIncrement = 2 * 3.14159f / verts;
    float angle = 0;
    for (int i = 0; i < verts; ++i) {
        Vertex a(x + cosf(angle) * r, y + sinf(angle) * r, z, currentColor.r, currentColor.g, currentColor.b, 0, 0);
        angle += angleIncrement;
        Vertex b(x + cosf(angle) * r, y + sinf(angle) * r, z, currentColor.r, currentColor.g, currentColor.b, 0, 0);
        pushVertex(center); pushVertex(a); pushVertex(b);
    }
}

void Camera::follow(glm::vec2 target, float speed, float timeStep) {
    // FIXME: Big integration error on low framerates!
    // FIXME: Speed is non-linear!
    focus = mix(focus, target, speed * timeStep);
}

glm::mat4 Camera::transform() const {
    glm::mat4 xform(1.0f);
    // Aspect ratio
    xform = glm::scale(xform, glm::vec3(1/aspect, 1, 1));
    // Scale depending on the zoom
    xform = glm::scale(xform, glm::vec3(1/verticalSize, 1/verticalSize, 0));
    // Translate to focus center
    xform = glm::translate(xform, glm::vec3(-focus, 0));
    return xform;
}

glm::vec2 Camera::project(glm::vec2 point) const {
    // XXX: Hack! use linear algebra
    point = glm::vec2(point.x * (16.f / 9.f), point.y);
    point = verticalSize * point;
    point = point + focus;
    return point;
}
