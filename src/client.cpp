// Copyright 2017, Stijn Brouwer
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "client.hpp"

#include "opengl.hpp"
#include "game.hpp"
#include "glad.h"
#include <SDL2/SDL.h>
#undef main
#include <time.h>
#include <vector>
#include <algorithm>

#define OPENGL_DEBUG

#ifdef OPENGL_DEBUG
#include <signal.h>
#define GL_DEBUG_OUTPUT 0x92E0
#define GL_DEBUG_OUTPUT_SYNCHRONOUS 0x8242
#define GL_DEBUG_SEVERITY_MEDIUM 0x9147
typedef void (APIENTRY *DEBUGPROC)(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar *message, const void *userParam);
typedef void (APIENTRY *SETCALLBACK)(DEBUGPROC proc, const void *userParam);
namespace {
    void APIENTRY debugCallback(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar *message, const void *userParam) {
        fprintf(stderr, "OpenGL Debug: %s\n", message);
    }
}
#endif

namespace {
    void initialize(SDL_Window **window, SDL_GLContext *context) {
        SDL_Init(SDL_INIT_VIDEO);
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
        *window = SDL_CreateWindow("liero", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 1280, 720, SDL_WINDOW_OPENGL);
        *context = SDL_GL_CreateContext(*window);
        gladLoadGL();
#ifdef OPENGL_DEBUG
        glEnable(GL_DEBUG_OUTPUT);
        glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
        auto glDebugMessageCallback = (SETCALLBACK) SDL_GL_GetProcAddress("glDebugMessageCallback");
        glDebugMessageCallback(debugCallback, nullptr);
#endif
        glEnable(GL_FRAMEBUFFER_SRGB);
    }

    void deinitialize(SDL_Window *window, SDL_GLContext context) {
        SDL_GL_DeleteContext(context);
        SDL_DestroyWindow(window);
        SDL_Quit();
    }
}

void clientMain() {
    SDL_Window *window;
    SDL_GLContext context;
    initialize(&window, &context);

    // FIXME: Gfx's destructor gets called after the GL context is destroyed, possible use after free of GL funptrs
    Gfx gfx;
    int tw = 8, th = 8;
    Terrain terrain(tw, th);
    gfx.updateTerrain(&terrain, true);
    srand((int) time(nullptr));
    constexpr int circleCount = 4;
    glm::vec2 circleCenters[circleCount];
    float circleRadii[circleCount];
    glm::vec2 center(64);
    float radius = 32;
    circleCenters[0] = center;
    circleRadii[0] = radius;
    for (int i = 1; i < circleCount; ++i) {
        float angle = rand() / (float) RAND_MAX * 2 * 3.14f;
        circleCenters[i] = center + glm::vec2(cosf(angle), sinf(angle)) * (radius * (rand() / (float) RAND_MAX * 0.5f + 0.5f));
        circleRadii[i] = radius / (1 + rand() / (float) RAND_MAX * 1);
    }
    terrain.carveCrater(circleCount, circleCenters, circleRadii);
    gfx.updateTerrain(&terrain);
    glm::vec2 fuck(54, 45);
    fuck = terrain.seekIsocurve(fuck, -2);

    Camera cam;
    cam.focus = glm::vec2(0);
    cam.verticalSize = 64;
    cam.aspect = 1280.f / 720.f;

    Player player;
    player.center = glm::vec2(64);
    player.radius = 2.0f;
    player.movementMode = Player::MovementMode::BALLISTIC;
    player.ballisticVelocity = glm::vec2(0);
    player.groundVelocity = 0;

    std::vector<Projectile> projectiles;

    for (;;) {
        SDL_Event ev;
        while (SDL_PollEvent(&ev)) {
            if (ev.type == SDL_QUIT) {
                goto end;
            }
        }

        glm::vec2 dir = glm::normalize(terrain.tangent(fuck));
        fuck -= dir * 8.0f;
        fuck = terrain.seekIsocurve(fuck, -2);

        int mouseX, mouseY;
        static bool wasShoot = false;
        auto mouseButtons = SDL_GetMouseState(&mouseX, &mouseY);
        if (mouseButtons & SDL_BUTTON(SDL_BUTTON_LEFT)) {
            if (!wasShoot) {
                glm::vec2 deviceCoords;
                deviceCoords.x = mouseX / 1280.f * 2 - 1;
                deviceCoords.y = mouseY / 720.f * 2 - 1;
                deviceCoords.y = -deviceCoords.y;
                glm::vec2 projected = cam.project(deviceCoords);
                glm::vec2 shootDirection = glm::normalize(projected - player.center);
                Projectile projectile;
                projectile.dead = false;
                projectile.center = player.center;
                projectile.radius = 1;
                projectile.explosivePower = 16;
                projectile.velocity = shootDirection * 150.f;
                projectiles.push_back(projectile);
            }
            wasShoot = true;
        }
        else {
            wasShoot = false;
        }

        if (!projectiles.empty()) {
            Projectile::simulate(projectiles.size(), &projectiles[0], &terrain, 1.f/60.f);
            projectiles.erase(std::remove_if(projectiles.begin(), projectiles.end(), [](const Projectile &proj) { return proj.dead; }), projectiles.end());
        }

        PlayerCommands com;
        static bool wasjump = false;
        auto keys = SDL_GetKeyboardState(nullptr);
        com.moveLeft = keys[SDL_SCANCODE_A] != 0;
        com.moveRight = keys[SDL_SCANCODE_D] != 0;
        com.jump = false;
        if (keys[SDL_SCANCODE_SPACE]) {
            if (!wasjump) {
                com.jump = true;
            }
            wasjump = true;
        }
        else {
            wasjump = false;
        }
        /*static int i = 0;
          i++;
          if (i % 8 == 0)*/
        Player::simulate(1, &player, &com, &terrain, 1.f/60);

        cam.follow(player.center, 6, 1.f / 60);
        //cam.follow(bodies[1].center, 6, 1.f / 60);
        //cam.verticalSize = glm::mix(cam.verticalSize, 32 + glm::length(bodies[1].velocity) / 4, 4./60);
        auto xform = cam.transform();

        gfx.updateTerrain(&terrain);

        glClearColor(0.2f, 0.3f, 0.4f, 1);
        glClear(GL_COLOR_BUFFER_BIT);
        gfx.clear();
        gfx.setColor(glm::vec3(0.2f));
        //gfx.quad(0, 0, 1, 1);
        gfx.flushUntextured(xform);
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        gfx.clear();
        gfx.setColor(glm::vec3(1.0f));
        gfx.quadSDFAligned(
            (float) tw * Terrain::CHUNK_SIZE / 2, (float) th * Terrain::CHUNK_SIZE / 2,
            (float) tw * Terrain::CHUNK_SIZE / 2, (float) th * Terrain::CHUNK_SIZE / 2,
            tw * Terrain::CHUNK_SIZE, th * Terrain::CHUNK_SIZE
        );
        gfx.flushSDF(xform, Texture::TERRAIN_SDF);
        glDisable(GL_BLEND);
        gfx.clear();
        gfx.setColor(glm::vec3(0.3f, 0.7f, 0.8f));
        gfx.circle(player.center.x, player.center.y, player.radius);
        for (const Projectile &projectile : projectiles) {
            gfx.setColor(glm::vec3(0.8f, 0.3f, 0.4f));
            gfx.circle(projectile.center.x, projectile.center.y, projectile.radius);
        }
        gfx.flushUntextured(xform);
        SDL_GL_SwapWindow(window);
    }
end:
    deinitialize(window, context);
}
