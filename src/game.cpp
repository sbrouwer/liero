// Copyright 2017, Stijn Brouwer
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "game.hpp"
#include <math.h>
#include <glm/glm.hpp>

Terrain::Terrain(int w, int h)
    : width(w), height(h)
    , texels(new float[width * height * CHUNK_SIZE * CHUNK_SIZE])
    , dirtyBits(new bool[width * height])
{
    for (int i = 0; i < width * height * CHUNK_SIZE * CHUNK_SIZE; ++i) {
        texels[i] = MAX_DISTANCE;
    }

    for (int i = 0; i < width * height; ++i) {
        dirtyBits[i] = false;
    }
}

namespace {
    // Signed distance function for a circle
    float sdfCircle(glm::vec2 point, glm::vec2 center, float radius) {
        return glm::distance(point, center) - radius;
    }

    int min(int a, int b) {
        if (a < b) return a;
        else return b;
    }

    int max(int a, int b) {
        if (a > b) return a;
        else return b;
    }

    // Linear interpolation between two floats
    float lerp(float a, float b, float t) {
        return a + (b - a) * t;
    }
}

float &Terrain::index(int x, int y) {
    return texels[x + y * CHUNK_SIZE * width];
}

float Terrain::index(int x, int y) const {
    return texels[x + y * CHUNK_SIZE * width];
}

float Terrain::sample(glm::vec2 position) const {
    // Calculate the indices of the texels we're going to interpolate
    glm::ivec2 min((int) position.x, (int) position.y);
    glm::ivec2 max((int) position.x + 1, (int) position.y + 1);
    // Calculate the parameters for the interpolation on each axis
    float modx = position.x - min.x;
    float mody = position.y - min.y;
    // Clamp the indices so we don't get out-of-bounds accesses
    if (min.x < 0) min.x = 0;
    if (min.y < 0) min.y = 0;
    if (max.x < 0) max.x = 0;
    if (max.y < 0) max.y = 0;
    if (min.x >= width * CHUNK_SIZE) min.x = width * CHUNK_SIZE - 1;
    if (min.y >= height * CHUNK_SIZE) min.y = height * CHUNK_SIZE - 1;
    if (max.x >= width * CHUNK_SIZE) max.x = width * CHUNK_SIZE - 1;
    if (max.y >= height * CHUNK_SIZE) max.y = height * CHUNK_SIZE - 1;
    // Index the points we're interpolating
    float bottomLeft = index(min.x, min.y);
    float bottomRight = index(max.x, min.y);
    float topRight = index(max.x, max.y);
    float topLeft = index(min.x, max.y);
    // Interpolate on the x axis
    float bottom = lerp(bottomLeft, bottomRight, modx);
    float top = lerp(topLeft, topRight, modx);
    // Interpolate the results on the y axis
    return lerp(bottom, top, mody);
}

glm::vec2 Terrain::gradient(glm::vec2 position) const {
    // TODO: Research if this epsilon value is supposed to be one,
    // or if this is just a magic number that seems to work correctly.
    constexpr float EPSILON = 1.0f;
    // Calculate the symmetric derivative on both axes going through the point
    float left = sample(position - glm::vec2(EPSILON, 0));
    float right = sample(position + glm::vec2(EPSILON, 0));
    float down = sample(position - glm::vec2(0, EPSILON));
    float up = sample(position + glm::vec2(0, EPSILON));
    float dx = (right - left) / (EPSILON * 2);
    float dy = (up - down) / (EPSILON * 2);
    return glm::vec2(dx, dy);
}

glm::vec2 Terrain::tangent(glm::vec2 position) const {
    auto normal = gradient(position);
    // Rotate the gradient 90 degrees CCW
    return glm::vec2(normal.y, -normal.x);
}

// XXX: Rewrite these root finding functions so they use the actual bisection method instead of the current hack

glm::vec2 Terrain::seekIsocurve(glm::vec2 point, float isovalue) const {
    constexpr int ITERATION_COUNT = 8;
    constexpr float EPSILON = 0.01f;

    // Approach the isocurve until our distance to it is lesser than or equal to EPSILON,
    // or our iteration count got exceeded (because there is no guarantee that this loop halts otherwise).
    for (int i = 0; i < ITERATION_COUNT; ++i) {
        if (fabsf(sample(point) - isovalue) <= EPSILON) {
            break;
        }
        point -= gradient(point) * (sample(point) - isovalue) * 0.5f;
    }

    return point;
}

float Terrain::seekIsocurve(glm::vec2 point, glm::vec2 normal, float isovalue) const {
    constexpr int ITERATION_COUNT = 8;
    constexpr float EPSILON = 0.01f;

    float coefficient = 0;

    // Approach the isocurve until our distance to it is lesser than or equal to EPSILON,
    // or our iteration count got exceeded (because there is no guarantee that this loop halts otherwise).
    for (int i = 0; i < ITERATION_COUNT; ++i) {
        if (fabsf(sample(point) - isovalue) <= EPSILON) {
            break;
        }
        float distance = sample(point);
        coefficient += (distance - isovalue) * 0.5f;
        point += normal * (distance - isovalue) * 0.5f;
    }

    return coefficient;
}

void Terrain::carveCrater(int circleCount, const glm::vec2 *centers, const float *radii) {
    if (circleCount <= 0) return;

    // Create a bounding box that will encompass all of our circles' distance fields
    glm::vec2 minimum = centers[0], maximum = centers[0];
    for (int i = 0; i < circleCount; ++i) {
        glm::vec2 circleMin = centers[i] - glm::vec2(radii[i] + MAX_DISTANCE);
        glm::vec2 circleMax = centers[i] + glm::vec2(radii[i] + MAX_DISTANCE);

        // Update the bounding box
        if (circleMin.x < minimum.x) minimum.x = circleMin.x;
        if (circleMin.y < minimum.y) minimum.y = circleMin.y;
        if (circleMax.x > maximum.x) maximum.x = circleMax.x;
        if (circleMax.y > maximum.y) maximum.y = circleMax.y;

        // Carve the circle into the signed distance field
        for (int y = max((int) circleMin.y, 0); y <= min((int) circleMax.y, height * CHUNK_SIZE - 1); ++y) {
            for (int x = max((int) circleMin.x, 0); x <= min((int) circleMax.x, width * CHUNK_SIZE - 1); ++x) {
                float circleDistance = sdfCircle(glm::vec2(x, y), centers[i], radii[i]);
                index(x, y) = fminf(index(x, y), circleDistance);
            }
        }
    }
    // Set each chunk that intersects our bounding box to dirty
    for (int y = max((int) (minimum.y / CHUNK_SIZE), 0); y <= min((int) (maximum.y / CHUNK_SIZE), height - 1); ++y) {
        for (int x = max((int) (minimum.x / CHUNK_SIZE), 0); x <= min((int) (maximum.x / CHUNK_SIZE), width - 1); ++x) {
            dirtyBits[x + y * width] = true;
        }
    }
}

void Terrain::carveCircle(glm::vec2 center, float radius) {
    carveCrater(1, &center, &radius);
}

void Projectile::simulate(int projectileCount, Projectile *projectiles, Terrain *terrain, float timeStep) {
    for (int i = 0; i < projectileCount; ++i) {
        Projectile &projectile = projectiles[i];

        if (projectile.dead) continue;

        projectile.velocity.y -= Ballistics::GRAVITY * timeStep;
        projectile.center += projectile.velocity * timeStep;

        float distance = -terrain->sample(projectile.center) - projectile.radius;
        if (distance <= 0) {
            projectile.dead = true;
            terrain->carveCircle(projectile.center, projectile.explosivePower);
        }
    }
}

void Player::simulate(int playerCount, Player *players, const PlayerCommands *commands, const Terrain *terrain, float timeStep) {
    for (int i = 0; i < playerCount; ++i) {
        Player &player = players[i];
        const PlayerCommands &com = commands[i];

        player.jumpGraceTimer += timeStep;

        // TODO: Ensure only one jump is done per frame
        // TODO: Make it so you can't cling to the walls just by pressing the movement keys

        if (player.movementMode == MovementMode::BALLISTIC) {
ballistic:
            if (player.jumpGraceTimer < JUMP_GRACE_PERIOD && com.jump) {
                player.jumpGraceTimer = JUMP_GRACE_PERIOD * 1000;
                player.ballisticVelocity.y = JUMP_VELOCITY;
            }

            float distance = -terrain->sample(player.center) - player.radius;
            if (distance <= WALLJUMP_THRESHOLD && com.jump) {
                player.ballisticVelocity.x += -glm::normalize(terrain->gradient(player.center)).x * JUMP_VELOCITY;
                player.ballisticVelocity.y = JUMP_VELOCITY;
            }

            float moveInput = 0;
            if (com.moveLeft) --moveInput;
            if (com.moveRight) ++moveInput;

            player.ballisticVelocity.x += moveInput * MOVEMENT_ACCELERATION * timeStep;
            player.ballisticVelocity.x -= player.ballisticVelocity.x * MOVEMENT_DRAG * timeStep;

            player.ballisticVelocity.y -= Ballistics::GRAVITY * timeStep;
            player.center += player.ballisticVelocity * timeStep;

            distance = -terrain->sample(player.center) - player.radius;
            if (distance <= 0) {
                auto normal = -glm::normalize(player.ballisticVelocity);
                player.center = terrain->seekIsocurve(player.center, -player.radius);

                float angle = atan2(normal.y, normal.x);
                if (angle > 0) {
                    player.movementMode = MovementMode::GROUND;
                    player.groundVelocity = player.ballisticVelocity.x;
                }
                else {
                    //player.ballisticVelocity = glm::reflect(player.ballisticVelocity, normal) * 0.6f;
                    player.ballisticVelocity = glm::vec2(0);
                }
            }
        }
        else if (player.movementMode == MovementMode::GROUND) {
            glm::vec2 normal = glm::normalize(terrain->gradient(player.center));

            float angle = atan2(normal.y, normal.x);
            if (angle > 0) {
                player.movementMode = MovementMode::BALLISTIC;
                glm::vec2 tangent(-normal.y, normal.x);
                player.ballisticVelocity = tangent * player.groundVelocity;
                player.jumpGraceTimer = 0;
                goto ballistic;
            }

            float distance = -terrain->sample(player.center) - player.radius;
            if (distance <= ON_GROUND_THRESHOLD) {
                player.center = terrain->seekIsocurve(player.center, -player.radius);
            }
            else {
                player.movementMode = MovementMode::BALLISTIC;
                glm::vec2 tangent(-normal.y, normal.x);
                player.ballisticVelocity = tangent * player.groundVelocity;
                player.jumpGraceTimer = 0;
                goto ballistic;
            }

            if (com.jump) {
                player.movementMode = MovementMode::BALLISTIC;
                player.ballisticVelocity = glm::vec2(0);
                player.ballisticVelocity.x = player.groundVelocity;
                player.ballisticVelocity -= glm::normalize(terrain->gradient(player.center)).x * JUMP_VELOCITY * JUMP_SURFACE_ANGLE_CONTRIB;
                player.ballisticVelocity.y = JUMP_VELOCITY;
                goto ballistic;
            }

            float moveInput = 0;
            if (com.moveLeft) --moveInput;
            if (com.moveRight) ++moveInput;

            if (angle < -3.14f / 2 - STRUGGLE_ANGLE) {
                if (moveInput < 0) {
                    moveInput *= -normal.y * normal.y;
                }
                else {
                    player.movementMode = MovementMode::BALLISTIC;
                    player.ballisticVelocity = glm::vec2(FALL_OFF_LEDGE_VELOCITY, 0);
                    player.jumpGraceTimer = 0;
                    goto ballistic;
                }
            }
            if (angle > -3.14f / 2 + STRUGGLE_ANGLE) {
                if (moveInput > 0) {
                    moveInput *= -normal.y * normal.y;
                }
                else {
                    player.movementMode = MovementMode::BALLISTIC;
                    player.ballisticVelocity = glm::vec2(-FALL_OFF_LEDGE_VELOCITY, 0);
                    player.jumpGraceTimer = 0;
                    goto ballistic;
                }
            }

            player.groundVelocity += moveInput * MOVEMENT_ACCELERATION * timeStep;
            player.groundVelocity -= player.groundVelocity * MOVEMENT_DRAG * timeStep;

            glm::vec2 tangent(-normal.y, normal.x);
            player.center += tangent * player.groundVelocity * timeStep;
        }
    }
}
