// Copyright 2017, Stijn Brouwer
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef LIERO_OPENGL_HPP
#define LIERO_OPENGL_HPP

#include <vector>
#include <stddef.h>
#include <glm/glm.hpp>

struct Vertex {
    glm::vec3 position;
    glm::vec3 color;
    glm::vec2 texCoord;

    Vertex() : position(), color(), texCoord() { }

    Vertex(float x, float y, float z, float r, float g, float b, float u, float v)
        : position(x, y, z)
        , color(r, g, b)
        , texCoord(u, v)
    { }
};

enum struct Texture {
    TERRAIN_SDF,

    COUNT
};

class Gfx {
    static constexpr size_t VERTEX_BUFFER_CAP = sizeof(Vertex) * 1024 * 1024;

    unsigned vertexArray;
    unsigned vertexBuffer;

    unsigned vertexShader;
    unsigned untexturedFragmentShader;
    unsigned untexturedProgram;
    unsigned sdfFragmentShader;
    unsigned sdfProgram;

    unsigned textures[(int) Texture::COUNT];

    std::vector<Vertex> vertexStack;
    glm::vec3 currentColor;

public:
    Gfx();
    ~Gfx();

    void updateTerrain(struct Terrain *terrain, bool force = false);

    void clear();

    void pushVertex(Vertex vertex);

    void flushUntextured(glm::mat4 transform);
    void flushSDF(glm::mat4 transform, Texture texture);

    void setColor(glm::vec3 color);
    void quad(float x, float y, float rx, float ry, float z = 0);
    void quadSDFAligned(float x, float y, float rx, float ry, int w, int h, float z = 0);
    void circle(float x, float y, float r, float z = 0, int verts = 24);
};

struct Camera {
    glm::vec2 focus;
    float verticalSize;
    float aspect;

    void follow(glm::vec2 target, float speed, float timeStep);

    glm::mat4 transform() const;

    glm::vec2 project(glm::vec2 point) const;
};

#endif
